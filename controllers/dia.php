<?php
/**
 * Created by PhpStorm.
 * User: Tomas
 * Date: 17/12/2018
 * Time: 15:47
 */

require_once __DIR__ . '/../repository/DiaRepository.php';

    $config=require_once __DIR__ . '/../app/config.php';
    App::bind('config',$config);
    $connection=App::getConnection();

    $dia = new DiaRepository();

    $dias = $dia->findAll();