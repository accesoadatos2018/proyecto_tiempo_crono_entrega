<?php
/**
 * Created by PhpStorm.
 * User: Tomas
 * Date: 15/12/2018
 * Time: 17:46
 */
return [
    'database' => ['name' => 'meteo',
        'username' => 'root',
        'password' => '',
        'connection' => 'mysql:host=localhost',
        'options' => [
            PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_PERSISTENT => true
        ]
    ]
];