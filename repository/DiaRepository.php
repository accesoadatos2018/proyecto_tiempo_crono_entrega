<?php
/**
 * Created by PhpStorm.
 * User: Tomas
 * Date: 17/12/2018
 * Time: 15:39
 */

require_once __DIR__ . '/../database/QueryBuilder.php';

class DiaRepository extends QueryBuilder
{

    public function __construct(string $table='dia', $classEntity='Dia')
    {
        parent::__construct($table, $classEntity);
    }
}