<!doctype html>
<html lang="en">
    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <link rel="stylesheet" href="../css/estils.css">
        <link rel="stylesheet" href="../css/bootstrap.min.css">

        <div class="knockout">Crono</div>

    </head>
<!--style="background-image: url(../../../../../foto.jpg)"-->
    <body >

        <div class="container">

            <table class="table table-hover table-bordered">
                <thead>
                    <tr>
                        <th>Día</th>
                        <th>Descripción</th>
                        <th>Estado Cielo</th>
                        <th>Periodo Estado Cielo</th>

                    </tr>
                </thead>
                <tbody>

                    <?php foreach ($dias as $dia) : ?>
                        <tr>
                            <th scope="row"><?= $dia->getFecha()?></th>
                            <th><?= $dia->getDescripcion()?></th>
                            <th><?= $dia->getEstadoCielo()?></th>
                            <th><?= $dia->getPeriodoEstadoCielo()?></th>
                        </tr>
                    <?php  endforeach;?>

                </tbody>
            </table>

<!--
            <div class="tablaPrecipitaciones">

                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Período</th>
                            <th>Probabilidad de Precipitación</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th>00-12</th>
                            <td>95</td>
                        </tr>
                        <tr>
                            <th>12-24</th>
                            <td>40</td>

                        </tr>
                        <tr>
                            <th>00-06</th>
                            <td>0</td>

                        </tr>
                        <tr>
                            <th>06-12</th>
                            <td>90</td>

                        </tr>
                        <tr>
                            <th>12-18</th>
                            <td>65</td>

                        </tr>
                        <tr>
                            <th>18-24</th>
                            <td>10</td>

                        </tr>
                    </tbody>
                </table>
            </div>-->

        </div>
        
    </body>
    
    <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
                crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
                crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
                crossorigin="anonymous"></script>

</html>