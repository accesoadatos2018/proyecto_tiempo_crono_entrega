<?php
/**
 * Created by PhpStorm.
 * User: Tomas
 * Date: 15/12/2018
 * Time: 17:50
 */
class Connection
{
    /**
     * @return PDO
     */
    public static function make()
    {
        $config=App::get('config')['database'];
        try {
            $connection = new PDO(
                $config['connection'] . ';dbname=' . $config['name'],
                $config['username'],
                $config['password'],
                $config['options']
            );
        }catch(PDOException $PDOException){
            die($PDOException->getMessage());
        }
        return $connection;
    }

}