<?php
/**
 * Created by PhpStorm.
 * User: Tomas
 * Date: 15/12/2018
 * Time: 18:10
 */
class Precipitacion{

    /**
     * @var string
     */
    private $periodo;

    /**
     * @var integer
     */
    private $probabilidad;

    /**
     * @var
     */
    private $fecha_dia;

    /**
     * Precipitacion constructor.
     * @param string $periodo
     * @param int $probabilidad
     * @param $fecha_dia
     */
    public function __construct(string $periodo, int $probabilidad, $fecha_dia)
    {
        $this->periodo = $periodo;
        $this->probabilidad = $probabilidad;
        $this->fecha_dia = $fecha_dia;
    }

    /**
     * @return string
     */
    public function getPeriodo(): string
    {
        return $this->periodo;
    }

    /**
     * @param string $periodo
     */
    public function setPeriodo(string $periodo): void
    {
        $this->periodo = $periodo;
    }

    /**
     * @return int
     */
    public function getProbabilidad(): int
    {
        return $this->probabilidad;
    }

    /**
     * @param int $probabilidad
     */
    public function setProbabilidad(int $probabilidad): void
    {
        $this->probabilidad = $probabilidad;
    }

    /**
     * @return mixed
     */
    public function getFechaDia()
    {
        return $this->fecha_dia;
    }

    /**
     * @param mixed $fecha_dia
     */
    public function setFechaDia($fecha_dia): void
    {
        $this->fecha_dia = $fecha_dia;
    }

}