<?php
/**
 * Created by PhpStorm.
 * User: Tomas
 * Date: 15/12/2018
 * Time: 18:00
 */
class Temperatura{

    /**
     * @var integer
     */
    private $maxima;

    /**
     * @var integer
     */
    private $minima;

    /**
     * Temperatura constructor.
     * @param int $maxima
     * @param int $minima
     */
    public function __construct(int $maxima, int $minima)
    {
        $this->maxima = $maxima;
        $this->minima = $minima;
    }

    /**
     * @return int
     */
    public function getMaxima(): int
    {
        return $this->maxima;
    }

    /**
     * @param int $maxima
     */
    public function setMaxima(int $maxima): void
    {
        $this->maxima = $maxima;
    }

    /**
     * @return int
     */
    public function getMinima(): int
    {
        return $this->minima;
    }

    /**
     * @param int $minima
     */
    public function setMinima(int $minima): void
    {
        $this->minima = $minima;
    }

}