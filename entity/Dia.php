<?php
/**
 * Created by PhpStorm.
 * User: Tomas
 * Date: 15/12/2018
 * Time: 17:54
 */

class Dia
{

    /**
     * @var string
     */
    private $fecha;

    /**
     * @var string
     */
    private $periodo_estado_cielo;

    /**
     * @var integer
     */
    private $estado_cielo;

    /**
     * @var string
     */
    private $descripcion;

    /**
     * @var integer
     */
    private $Temperatura;

    /**
     * Dia constructor.
     * @param string $fecha
     * @param int $periodoEstadoCielo
     * @param int $estadoCielo
     * @param string $descripcion
     * @param int $Temperatura
     */
    public function __construct(string $fecha='', string $periodoEstadoCielo='', int $estadoCielo=0, string $descripcion='', int $Temperatura=0)
    {
        $this->fecha = $fecha;
        $this->periodo_estado_cielo = $periodoEstadoCielo;
        $this->estado_cielo = $estadoCielo;
        $this->descripcion = $descripcion;
        $this->Temperatura = $Temperatura;
    }

    /**
     * @return string
     */
    public function getFecha(): string
    {
        return $this->fecha;
    }

    /**
     * @param string $fecha
     */
    public function setFecha(string $fecha): void
    {
        $this->fecha = $fecha;
    }

    /**
     * @return string
     */
    public function getPeriodoEstadoCielo(): string
    {
        return $this->periodo_estado_cielo;
    }

    /**
     * @param int $periodo_estado_cielo
     */
    public function setPeriodoEstadoCielo(int $periodo_estado_cielo): void
    {
        $this->periodo_estado_cielo = $periodo_estado_cielo;
    }

    /**
     * @return int
     */
    public function getEstadoCielo(): int
    {
        return $this->estado_cielo;
    }

    /**
     * @param int $estado_cielo
     */
    public function setEstadoCielo(int $estado_cielo): void
    {
        $this->estado_cielo = $estado_cielo;
    }

    /**
     * @return string
     */
    public function getDescripcion(): string
    {
        return $this->descripcion;
    }

    /**
     * @param string $descripcion
     */
    public function setDescripcion(string $descripcion): void
    {
        $this->descripcion = $descripcion;
    }

    /**
     * @return int
     */
    public function getTemperatura(): int
    {
        return $this->Temperatura;
    }

    /**
     * @param int $Temperatura
     */
    public function setTemperatura(int $Temperatura): void
    {
        $this->Temperatura = $Temperatura;
    }

    public function toArray(): array
    {

        return[
            'fecha' => $this->getFecha(),
            'periodo_estado_cielo' => $this->getPeriodoEstadoCielo(),
            'estado_cielo' => $this->getEstadoCielo(),
            'descripcion' => $this->getDescripcion()
        ];

    }
}
